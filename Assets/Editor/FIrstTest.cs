﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;

public class FIrstTest {

    [Test]
    public void とりあえずテスト()
    {
        var ゲームオブジェクト = new GameObject();
		Undo.RegisterCreatedObjectUndo (ゲームオブジェクト, "ゲームオブジェクト作成");

        var newGameObjectName = "新しいゲームオブジェクトの設定";
        ゲームオブジェクト.name = newGameObjectName;

        Assert.AreEqual(newGameObjectName, ゲームオブジェクト.name);
    }
}
